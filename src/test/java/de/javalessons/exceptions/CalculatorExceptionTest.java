package de.javalessons.exceptions;

import de.javalessons.homework43.BankAccount;
import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorExceptionTest {

    private LogCaptor logCaptor = LogCaptor.forClass(CalculatorException.class);


    @DisplayName("Тестирую деление двух чисел без отклонений")
    @Test
    void testDivideByZeroThrowsArithmeticException() {
        CalculatorException calculatorException = new CalculatorException();
        Assertions.assertThrows(WrongArgumentsException.class,
                () -> calculatorException.divide("30",0));
    }

    @Test
    void testDivideByZeroThrowsNumberFormatException() {
        CalculatorException calculatorException = new CalculatorException();
        Assertions.assertThrows(WrongArgumentsException.class,
                () -> calculatorException.divide("30A",0));
    }

    @Test
    void testDivideSuccess(){
        CalculatorException calculatorException = new CalculatorException();
        Assertions.assertEquals(3,
                calculatorException.divide("90", 30) );
    }

    @Test
    void testDivideByZeroThrowsArithmeticExceptionCheckLog(){
        logCaptor.setLogLevelToInfo();
        CalculatorException calculatorException = new CalculatorException();
        String expectedInfoMessage = "Divide by 0";
        Assertions.assertThrows(WrongArgumentsException.class,
                () -> calculatorException.divide("30",0));
        Assertions.assertTrue(logCaptor.getErrorLogs().contains(expectedInfoMessage));
    }

    @Test
    void testShouldLogErrorWhenDividendIsNonNumeric(){
        logCaptor.setLogLevelToInfo();
        CalculatorException calculatorException = new CalculatorException();
        String expectedInfoMessage = "Wrong format, not number in String";
        Assertions.assertThrows(WrongArgumentsException.class,
                () -> calculatorException.divide("30DD",20));
        Assertions.assertFalse(logCaptor.getErrorLogs().contains(expectedInfoMessage));
    }
}