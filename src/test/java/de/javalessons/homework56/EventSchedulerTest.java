package de.javalessons.homework56;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class EventSchedulerTest {

    private EventScheduler eventScheduler;

    @BeforeEach
    public void setUp() {
        eventScheduler = new EventScheduler();

    }

    @Test
    void testAddEvent() {
        LocalDateTime eventFilmTerminatorStart = LocalDateTime.of(2023, 12, 05, 20, 0);
        LocalDateTime eventFilmTerminatorEnd = LocalDateTime.of(2023, 12, 05, 22, 0);
        Event eventFilmTerminator = new Event("Film Terminator", eventFilmTerminatorStart, eventFilmTerminatorEnd);
        Assertions.assertEquals(0, eventScheduler.getAllEvents().size());
        eventScheduler.addEvent(eventFilmTerminator);
        Assertions.assertEquals(1, eventScheduler.getAllEvents().size());
        Event checkEvent = eventScheduler.getAllEvents().get(0);
        Assertions.assertEquals(eventFilmTerminator, checkEvent);
    }

    @Test
    void testDeleteEvent() {
        LocalDateTime eventFilmTerminatorStart = LocalDateTime.of(2023, 12, 05, 20, 0);
        LocalDateTime eventFilmTerminatorEnd = LocalDateTime.of(2023, 12, 05, 22, 0);
        Event eventFilmTerminator = new Event("Film Terminator", eventFilmTerminatorStart, eventFilmTerminatorEnd);
        Assertions.assertEquals(0, eventScheduler.getAllEvents().size());
        eventScheduler.addEvent(eventFilmTerminator);
        eventScheduler.deleteEvent(eventFilmTerminator);
        Assertions.assertEquals(0, eventScheduler.getAllEvents().size());
    }
}