package de.javalessons.homework13;

import java.util.ArrayList;

public class Aufgabe04 {
    public static void main(String[] args) {
        /**
         * Создайте два ArrayList со строками.
         * Найдите элементы, которые присутствуют в
         * обоих списках и создайте новый список на их основе.
         * Выведите новый список.
         */
        ArrayList<String> listString1 = new ArrayList<>();
        ArrayList<String> listString2 = new ArrayList<>();
        ArrayList<String> result = new ArrayList<>();
        listString1.add("one");
        listString1.add("two");
        listString1.add("three");
        listString1.add("seven");
        listString1.add("one");
        listString2.add("three");
        listString2.add("two");
        listString2.add("four");
        listString2.add("one");
        for (int j = 0; j < listString2.size(); j++) {
            for (int i = 0; i < listString1.size(); i++) {
                String checkElement = listString1.get(i);
                String elementInListArray = listString2.get(j);
                if (checkElement.equals(elementInListArray) && !result.contains(checkElement)) {
                    result.add(checkElement);
                }
            }
        }
        System.out.println(result);


        ArrayList<String> list1 = new ArrayList<>();
        list1.add("Hello");
        list1.add("Good");
        list1.add("Дождь");
        list1.add("Страна");
        list1.add("Ветер");
        list1.add("Солнце");


        ArrayList<String> list2 = new ArrayList<>();
        list2.add("Ночь");
        list2.add("Ветер");
        list2.add("Небо");
        list2.add("Java");
        list2.add("Good");
        list2.add("Ветер");//TODO проверить на то, что элемент уже добавлен
        System.out.println(list1);
        System.out.println(list2);

        ArrayList<String> temp = new ArrayList<>();
        for (String element1:list1 ){
            for (String element2:list2){
                if (element1.equals(element2)){
                    temp.add(element1);
                }
            }
        }
        System.out.println("Общие элементы: " + temp);
    }
}
