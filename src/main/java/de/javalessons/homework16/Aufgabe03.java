package de.javalessons.homework16;

import java.util.ArrayList;

public class Aufgabe03 {
    /*Задание 3: Создайте метод, который принимает массив целых чисел и
        возвращает среднее арифметическое этих чисел.*/
    public static void main(String[] args) {

    double average = calculateAverage();
        System.out.println("Среднее арифметическое: " + average);
}

    static double calculateAverage() {
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            numbers.add(i);
        }
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        return (double) sum / numbers.size();
    }
}
