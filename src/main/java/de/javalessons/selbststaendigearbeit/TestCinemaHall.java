package de.javalessons.selbststaendigearbeit;

import de.javalessons.javaio.ScheduleConflictException;

import java.time.LocalDateTime;

public class TestCinemaHall {
    public static void main(String[] args) {
        CinemaHall cinemaHall = new CinemaHall(1);
        LocalDateTime startTimeMovie = LocalDateTime.of(2023, 12, 20, 20, 00);
        LocalDateTime endTimeMovie = LocalDateTime.of(2023, 12, 20, 22, 00);
        Movie movie = new Movie("Batman", startTimeMovie, endTimeMovie, 100);


        try {
            cinemaHall.addMovie(movie);
            cinemaHall.bookTicket("Batman", 50);
            cinemaHall.bookTicket("Batman", 30);
            cinemaHall.bookTicket("Batman-2", 10);
            cinemaHall.bookTicket("Batman", 30);
        } catch (ScheduleConflictException scheduleConflictException) {
            System.out.println(scheduleConflictException + "Error. Schedule conflict. Movie:" + movie.getTitle());
        } catch (SoldOutException soldOutException) {
            System.out.println(soldOutException + "Sold out !!!  " + movie.getTitle());
        }
    }
}
