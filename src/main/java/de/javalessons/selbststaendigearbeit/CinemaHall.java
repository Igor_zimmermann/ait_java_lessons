package de.javalessons.selbststaendigearbeit;

import de.javalessons.javaio.EmployeeInputStream;
import de.javalessons.javaio.ScheduleConflictException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CinemaHall {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaHall.class);


    private int hallNumber;

    private List<Movie> moviesSchedule;

    public CinemaHall(int hallNumber) {
        this.hallNumber = hallNumber;
        this.moviesSchedule = new ArrayList<>();
    }

    public void addMovie(Movie movie) throws ScheduleConflictException {
        if (isScheduleConflict(movie.getStartTime(), movie.getEndTime())) {
            moviesSchedule.add(movie);
            LOGGER.info("Movie {} was added to the Movie List", movie.getTitle());
        } else {
            LOGGER.error("Schedule conflict {}", movie.getTitle());
            throw new ScheduleConflictException("Schedule conflict " + movie.getTitle());
        }
    }

    public boolean isScheduleConflict(LocalDateTime newMovieStart, LocalDateTime newMovieEnd) {
        if (newMovieStart.isAfter(newMovieEnd)) {
            LOGGER.error("Wrong params {} {}", newMovieStart.toString(), newMovieEnd.toString());
            throw new IllegalArgumentException("Wrong params " + newMovieStart.toString() + newMovieEnd.toString());
        }

        boolean result = true;
        for (Movie movie : moviesSchedule) {
            if (newMovieStart.isBefore(movie.getEndTime()) && newMovieEnd.isAfter(movie.getStartTime())) {
            } else {
                result = false;
            }
            //  return (newMovieStart.isBefore(movie.getEndTime()) && newMovieEnd.isAfter(movie.getStartTime())){
        }

        return result;
    }

    public void bookTicket(String title, int tickets) throws SoldOutException {
        boolean movieWasFound = false;
        for (Movie movie : moviesSchedule) {
            if (title.equals(movie.getTitle())) {
                movieWasFound = true;
                if (movie.getAvailableTickets() >= tickets) {
                    movie.setAvailableTickets(movie.getAvailableTickets() - tickets);
                    LOGGER.info(" Movie: {} Available tickets {}", movie.getTitle(), movie.getAvailableTickets());
                } else {
                    throw new SoldOutException("Not enough tickets available Available: " + movie.getAvailableTickets() + " Ticket: " + tickets);
                }
            }

        }
        if (!movieWasFound) {
            LOGGER.warn("Movie was not found {}", title);
        }
    }


}
