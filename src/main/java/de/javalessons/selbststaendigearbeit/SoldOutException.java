package de.javalessons.selbststaendigearbeit;

public class SoldOutException extends Exception{
    public SoldOutException(String message) {
        super(message);
    }
}
