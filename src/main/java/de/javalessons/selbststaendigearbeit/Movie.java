package de.javalessons.selbststaendigearbeit;

import java.time.LocalDateTime;

public class Movie {

    private String title;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    private int availableTickets;

    public Movie(String title, LocalDateTime startTime, LocalDateTime endTime, int availableTickets) {
        this.title = title;
        this.startTime = startTime;
        this.endTime = endTime;
        this.availableTickets = availableTickets;
    }

    public String getTitle() {
        return title;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public int getAvailableTickets() {
        return availableTickets;
    }

    public void setAvailableTickets(int availableTickets) {
        this.availableTickets = availableTickets;
    }
}
