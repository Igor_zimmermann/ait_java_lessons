package de.javalessons.homework56;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class TestEvents {

    private static EventScheduler eventScheduler = new EventScheduler();

    public static void main(String[] args) {
        LocalDateTime eventFilmTerminatorStart = LocalDateTime.of(2023, 12, 05, 20, 0);
        LocalDateTime eventFilmTerminatorEnd = LocalDateTime.of(2023, 12, 05, 22, 0);
        Event eventFilmTerminator = new Event("Film Terminator", eventFilmTerminatorStart, eventFilmTerminatorEnd);

        LocalDateTime eventFilmRemboStart = LocalDateTime.of(2023, 12, 05, 21, 0);
        LocalDateTime eventFilmRemboEnd = LocalDateTime.of(2023, 12, 05, 23, 0);
        Event eventFilmRembo = new Event("Film Rembo", eventFilmRemboStart, eventFilmRemboEnd);

        eventScheduler.addEvent(eventFilmTerminator);
        eventScheduler.addEvent(eventFilmRembo);

        List<Event> allEvents = eventScheduler.getAllEvents();
         for (Event event: allEvents){
         System.out.println(event.toString());
         }

        LocalDate localDate = LocalDate.of(2023, 12, 6);
        List<Event> result = eventScheduler.getEventsOfDateTwo(localDate);
        for (Event event : result) {
            System.out.println(event.toString());
        }


        boolean checkOverlape = eventScheduler.checkEventsToOverlape(eventFilmRembo, eventFilmTerminator);
        System.out.println(checkOverlape);


        eventScheduler.deleteEvent(eventFilmRembo);

        List<Event> resultAfterDelete = eventScheduler.getAllEvents();
        for (Event event : resultAfterDelete) {
            System.out.println(event.toString());
        }

    }
}
