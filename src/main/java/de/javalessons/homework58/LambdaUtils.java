package de.javalessons.homework58;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LambdaUtils {

    public static void main(String[] args) {
        //Создайте список строк. Используя лямбда-выражения и метод filter из Stream API,
        // отфильтруйте и выведите на экран все строки, которые начинаются на букву "А".
        List<String> cars = Arrays.asList("VW", "BMW", "Skoda", "Alfa-Romeo", "Seat", "Audi");
        cars.stream().filter(car -> car.startsWith("A") || car.startsWith("S")).forEach(System.out::println);

        //Имеется список целых чисел. Используя лямбда-выражение, преобразуйте
        // каждое число в его квадрат и сохраните результаты в новом списке.
        List<Integer> numbers = Arrays.asList(2, 6, 9, 10);
        List<Integer> result = numbers.stream().map(number -> number * number).toList();
        numbers.forEach(System.out::println);
        System.out.println("--------");
        result.forEach(System.out::println);
        System.out.println("--------");

        //Используя поток данных (Stream) из списка целых чисел, напишите лямбда-выражение
        // для фильтрации всех четных чисел и их последующего вывода на экран.
        List<Integer> numbersToSelect = Arrays.asList(10, 55, 70, 33, 4);
        numbersToSelect.stream().filter(selectedNumber -> selectedNumber % 2 == 0)
                .forEach(System.out::println);
        System.out.println("--------");

        //Имеется список целых чисел. Используя лямбда-выражение
        // в методе forEach, напечатайте каждый элемент списка.
        List<Integer> numbersToPrint = Arrays.asList(10, 55, 70, 33, 4);
        numbersToPrint.stream().forEach(System.out::println);
        System.out.println(numbersToPrint.stream().toList());
        System.out.println("--------");

        //Используйте потоки для обработки коллекций с помощью лямбда-выражений,
        // преобразуйте все строки в списке в верхний регистр.
        List<String> stringsToUpperCase = Arrays.asList("Apple", "Samsung", "Sony", "LG", "Huawei");
        //stringsToUpperCase.stream().map(s -> s.toUpperCase()).forEach(System.out::println);
        stringsToUpperCase.stream().map(String::toUpperCase).sorted().forEach(System.out::println);

        List<String> upperCaseStrings = stringsToUpperCase.stream()
                .map(String::toUpperCase) // Используем метод toUpperCase для преобразования строки в верхний регистр
                .collect(Collectors.toList()); // Собираем результат в список
        System.out.println("--------");

        List<String> cities = Arrays.asList("Berlin", "London", "Paris", "Madrid", "Lisbon");
        //запускаем поток-->фильтруем те что начинаются на L-->перевод в верхний регистр-->сортировать по порядку-->вывод на консоль
        cities.stream().filter(capital -> capital.startsWith("L"))
                .map(String::toUpperCase)
                .sorted()
                .forEach(System.out::println);

        System.out.println("--------");


        Optional<String> resultSelect = cities.stream().filter(capital -> capital.startsWith("L")).findFirst()
                .map(String::toUpperCase);
        if(resultSelect.isPresent()){
            System.out.println(resultSelect.get());
        }


    }
}
