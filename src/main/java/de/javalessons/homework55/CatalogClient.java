package de.javalessons.homework55;

public class CatalogClient {

    public static void main(String[] args) {

        FashionShop fashionShop = new FashionShop();
        FashionItem fashionItemWatch = new FashionItem("Часы", 2000,
                FashionCategory.АКСЕССУАРЫ, Season.ВСЕСЕЗОННАЯ);
        FashionItem fashionItemCoat = new FashionItem("Пальто",250,
                FashionCategory.ОДЕЖДА, Season.ЗИМА);
        FashionItem fashionItemBoots = new FashionItem("Туфли", 500, FashionCategory.ОБУВЬ, Season.ЛЕТО);

        fashionShop.addFashionItem(fashionItemWatch);
        fashionShop.addFashionItem(fashionItemCoat);
        fashionShop.addFashionItem(fashionItemBoots);

        fashionShop.showInfoAboutSeason(Season.ВСЕСЕЗОННАЯ);
        fashionShop.showInfoAboutSeason(Season.ЛЕТО);
        fashionShop.showInfoAboutSeason(Season.ЗИМА);
        fashionShop.showInfoAboutSeason(Season.ВЕСНА);

    }
}
