package de.javalessons.homework55;

public class FashionItem {

    private String name;

    private double preis;

    private FashionCategory fashionCategory;

    private Season season;

    public FashionItem(String name, double preis,
                       FashionCategory fashionCategory, Season season) {
        this.name = name;
        this.preis = preis;
        this.fashionCategory = fashionCategory;
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public FashionCategory getFashionCategory() {
        return fashionCategory;
    }

    public void setFashionCategory(FashionCategory fashionCategory) {
        this.fashionCategory = fashionCategory;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    @Override
    public String toString() {
        return
                "название:" + name  +
                ", цена:" + preis +
                ", категория:" + fashionCategory +
                ", сезон:" + season;
    }
}
