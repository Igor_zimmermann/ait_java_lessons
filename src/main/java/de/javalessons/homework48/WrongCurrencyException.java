package de.javalessons.homework48;

public class WrongCurrencyException extends RuntimeException {
    public WrongCurrencyException(String message) {
        super(message);
    }
}
