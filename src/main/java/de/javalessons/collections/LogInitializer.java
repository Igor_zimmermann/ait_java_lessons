package de.javalessons.collections;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LogInitializer {

    public static void ensureLogDirectoryExists() {
        Path logDir = Paths.get("./logs");
        Path archivedLogDir = Paths.get("./logs/archived");

        try {
            if (!Files.exists(logDir)) {
                Files.createDirectories(logDir);
            }

            if (!Files.exists(archivedLogDir)) {
                Files.createDirectories(archivedLogDir);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Обработка ошибок, если не удается создать каталоги
        }
    }
}
