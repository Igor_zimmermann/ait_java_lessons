package de.javalessons.collections;

import de.javalessons.zooshop.Dog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CollectionSet {

    private static final Logger LOGGER = LoggerFactory.getLogger(CollectionSet.class);


    public static void main(String[] args) {

        List<String> stringList = new ArrayList<>();
        stringList.add("Audi");
        stringList.add("VW");
        stringList.add("Audi");
        LOGGER.info("Audi was added");
        LOGGER.error("Audi was added");


        Dog bulldog = new Dog("Guffi", "bulldog");
        Dog dachshund = new Dog("Emma", "dachshund");
        Dog dachshundNew = new Dog("Emma", "dachshund");


        Set<Dog> dogSet = new HashSet<>();
        dogSet.add(bulldog);
        dogSet.add(dachshund);
        dogSet.add(dachshundNew);

        for (Dog dog: dogSet){
            System.out.println(dog.getName() + " " + dog.getName().hashCode());
            System.out.println(dog.getBreed() +  " " + dog.getBreed().hashCode());
            System.out.println(dog.hashCode());
            System.out.println("---------");
        }

        System.out.println("---------");

        bulldog.setName("Muffi");
        for (Dog dog: dogSet){
            System.out.println(dog.getName() + " " + dog.getName().hashCode());
            System.out.println(dog.getBreed() +  " " + dog.getBreed().hashCode());
            System.out.println(dog.hashCode());
            System.out.println("---------");
        }



        Set<String> stringSet = new HashSet<>();
        stringSet.add("Alex");
        stringSet.add("Anna");
        stringSet.add("Max");
        stringSet.add("Maxim");

        for (String name: stringSet){
            System.out.println(name);
            System.out.println(name.hashCode());
        }
        System.out.println("---------");

        stringSet.remove("Maxim");

        stringSet.clear();

        System.out.println("---------");
        stringSet.add("Alex");
        for (String name: stringSet){
            System.out.println(name);
            System.out.println(name + " " +name.hashCode());

        }


        System.out.println("---------");

        Iterator<String> iterator = stringSet.iterator();
        while (iterator.hasNext()){
            String element = iterator.next();
            System.out.println(element);
            System.out.println(element.hashCode());
        }

    }
}
