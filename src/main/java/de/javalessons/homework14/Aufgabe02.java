package de.javalessons.homework14;

import java.util.ArrayList;
import java.util.Scanner;

public class Aufgabe02 {
    public static void main(String[] args) {
        /**
         * Создайте ArrayList, который содержит несколько
         * дублирующихся элементов.
         * Напишите код, который удаляет дубликаты из списка.
         * Выведите отредактированный список на экран.
         */
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> fruits = new ArrayList<>();
        ArrayList<String> fruitsNew = new ArrayList<>();
        boolean newInput = true;
        while (newInput) {
            System.out.println(" Enter a fruit");
            String fruit = scanner.next();
            fruits.add(fruit);
            System.out.println(" Would You like to enter the next position? Enter y to continue and n to stop.");
            String answer = scanner.next();
            if (answer.equalsIgnoreCase("n")) {
                newInput = false;
            }
            for (String fruitWare : fruits) {
                if (!fruitsNew.contains(fruitWare)) {
                    fruitsNew.add(fruitWare);
                }
            }
            System.out.println(fruitsNew);
        }

        //#2
        ArrayList<Integer> numbers = new ArrayList<>();
        //создаем  дубликаты и елеметы в нашем массиве
        for (int i = 1; i <= 7; i++) {
            numbers.add(i);
        }
        numbers.add(2);
        numbers.add(5);
        numbers.add(6);
        numbers.add(6);
        for (int num : numbers) { //бежим по нему и выводим каждое число отдельно в строку
            System.out.print(num + " ");
        }
        for (int i = 0; i < numbers.size(); i++) { //проходим по всем индексам
            Integer currentNum = numbers.get(i); //берем каждое число отдельно а затем делаем с ним то что во внутреннем цикле
            for (int j = i + 1; j < numbers.size(); j++) { //внутренний цикл работает с числом и сравнивает каждое следующее число в массиве на наличие дубликатов
                if (currentNum.equals(numbers.get(j))) { //если проверочное число совпадает с числом в массиве
                    numbers.remove(j); //встреченое число удаляется
                    j--; // сдвигаем индексы после удаления
                }
            }
        }
        System.out.println();
        for (int num : numbers) { //выводим каждое число отдельно в строку
            System.out.print(num + " ");
        }
    }

}
