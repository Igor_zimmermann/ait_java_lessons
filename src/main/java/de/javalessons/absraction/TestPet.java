package de.javalessons.absraction;

import java.util.Objects;

public class TestPet {
    public static void main(String[] args) {
        Pet pet =  new Dog();
        pet.makeSound();
        pet = new Cat();


        pet.makeSound();
        pet.setName("Milli");
        pet.showName();

        Dog dogOne = new Dog(12);
        Dog dogTwo = new Dog(12);

        System.out.println(Objects.equals(dogOne, dogTwo));
        System.out.println(dogOne.equals(dogTwo));

        Object a = new Dog();
        Object b = new Dog();
        System.out.println(a.equals(b)) ;


    }
}
