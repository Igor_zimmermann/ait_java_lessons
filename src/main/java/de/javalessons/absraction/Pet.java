package de.javalessons.absraction;

abstract class Pet {
   String name;

   abstract void makeSound();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void showName(){
        System.out.println(name);
    }
}
