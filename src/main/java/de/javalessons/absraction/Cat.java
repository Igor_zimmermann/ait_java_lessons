package de.javalessons.absraction;

public class Cat extends Pet {

    @Override
    void makeSound() {
        System.out.println("Cat says mjau");

    }

}
