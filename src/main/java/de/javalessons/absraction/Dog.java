package de.javalessons.absraction;

import java.util.Objects;

public class Dog extends Pet{

    int age;

    public Dog(int age) {
        this.age = age;
    }

    public Dog() {
    }

    @Override
    void makeSound() {
        System.out.println("Dog says woof");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return age == dog.age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(age);
    }
}
