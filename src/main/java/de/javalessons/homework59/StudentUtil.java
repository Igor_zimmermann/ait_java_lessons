package de.javalessons.homework59;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class StudentUtil {
    private static Student studentBatman = new Student("John", "Batman", 45, 1.7);
    private static Student studentRobin = new Student("Robin", "Hill", 15, 1.2);
    private static Student studentTerminator = new Student("Arnold", "A-1000", 15, 1.0);
    private static List<Student> studentList;
    public static void main(String[] args) {
        studentList = Arrays.asList(studentBatman, studentRobin, studentTerminator);

        studentList.sort((studentOne, studentTwo) -> studentOne.getLastname().compareTo(studentTwo.getLastname()));

        Student bestStudent = studentList.stream().reduce((studentOne, studentTwo) ->
                        studentOne.getAverageGrade() > studentTwo.averageGrade ? studentOne : studentTwo)
                .orElse(null);
        Student lastStudent = studentList.stream().reduce((studentOne, studentTwo) ->
                        studentOne.getAverageGrade() < studentTwo.averageGrade ? studentOne : studentTwo)
                .orElse(null);
        System.out.println("lastStudent-->" + lastStudent);
        System.out.println("bestStudent-->" + bestStudent);

        double averageGradeToCheck = 1.2;

        studentList.stream().filter(student -> student.getAverageGrade() >= averageGradeToCheck).toList()
                .forEach(System.out::println);

        List<String> studentNames = studentList.stream()
                .map(Student::getFirstname)
                .toList();
        System.out.println(studentNames);

        int ageToCheck = 20;
        long countResult = studentList.stream().filter(student -> student.getAge() > ageToCheck).count();
        System.out.println(countResult);
    }
}
