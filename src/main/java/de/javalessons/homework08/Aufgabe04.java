package de.javalessons.homework08;

import java.util.Scanner;

public class Aufgabe04 {

    public static void main(String[] args) {
        /**
         * Банковский аккаунт: Создайте программу, которая имитирует работу банковского аккаунта.
         * Пользователь может выбирать действия, такие как внесение средств,
         * снятие средств и проверка баланса. Используйте цикл do-while,
         * чтобы продолжать работу с аккаунтом до тех пор, пока пользователь не решит выйти.
         */
        Scanner sc = new Scanner(System.in);
        String vneseno = "Внесение средств-1 ";
        String snato = "Снятие средств-2 ";
        String proverka = "Проверка баланса-3 ";
        String vihod = "Выход-0";
        String operation;
        int vklad = 20;
        int status;
        System.out.println(vneseno + snato + proverka + vihod);
        do {
            status = sc.nextInt();
            switch (status) {
                case 1:
                    operation = "Внесение средств";
                    System.out.println(operation);
                    System.out.println("Какую сумму желаете внести?");
                    vklad = vklad + sc.nextInt();
                    System.out.printf("На Вашем счету %d euro", vklad);
                    break;
                case 2:
                    operation = "Снятие средств";
                    System.out.println(operation);
                    System.out.println("Какую сумму желаете снять?");
                    int rashod = sc.nextInt();
                    if (rashod > vklad) {
                        System.out.println("У вас не хватит на это денег");
                    } else {
                        vklad = vklad - rashod;
                    }
                    System.out.printf("На Вашем счету %d euro", vklad);
                    break;
                case 3:
                    operation = "Проверка баланса";
                    System.out.println(operation);
                    System.out.printf("Ваш баланс %d euro", vklad);
                    break;
                case 0:
                    operation = "Выход из системы";
                    System.out.println(operation);
                    break;
                default:
                    System.err.println("wrong input");
                    status = 0;
            }

        }
        while (status != 0);

    }

}
