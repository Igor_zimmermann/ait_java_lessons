package de.javalessons.enums;


import java.util.EnumMap;
import java.util.EnumSet;

public class Main {

    public static void main(String[] args) {
        DayUtil today = new DayUtil(29, Day.WEDNESDAY, Seasons.WINTER);
        DayUtil tomorrow = new DayUtil(30, Day.WEDNESDAY, Seasons.WINTER);

        System.out.println(today.getSeason().ordinal());
        System.out.println(today.getDayOfWeek().ordinal());
        System.out.println(Seasons.values()[0]);
        System.out.println(Seasons.valueOf("WINTER").getDescription());

        System.out.println(today.dayOfWeek.equals(Day.FRIDAY));


        switch (today.dayOfWeek) {
            case MONDAY -> System.out.println("НЕТ");
            case WEDNESDAY -> System.out.println("ДА");
            default -> System.out.println("Не знаю");
        }


        EnumMap<Day, String> dayStringEnumMap = new EnumMap<>(Day.class);

        dayStringEnumMap.put(Day.SATURDAY, "Выходной");
        dayStringEnumMap.put(Day.TUESDAY, "Уже не понедельник");
        dayStringEnumMap.put(Day.MONDAY, "Не хочу на работу");
        dayStringEnumMap.put(Day.FRIDAY, "Ура пятница!!!");
        dayStringEnumMap.put(Day.FRIDAY, "Еще пятница :-(");


        System.out.println(dayStringEnumMap.size());
        System.out.println(dayStringEnumMap.get(Day.FRIDAY));
        //System.out.println(dayStringEnumMap.remove(Day.MONDAY));
        System.out.println(dayStringEnumMap.containsKey(Day.MONDAY));
        System.out.println(dayStringEnumMap.containsKey(Day.TUESDAY));
        System.out.println(dayStringEnumMap.containsValue("Не хочу на работу"));
        System.out.println(dayStringEnumMap.values());
        System.out.println(dayStringEnumMap.get(Day.FRIDAY).equals(dayStringEnumMap.get(Day.MONDAY)));

        EnumSet<Day> weekend = EnumSet.of(Day.SATURDAY, Day.SUNDAY);
        EnumSet<Day> weekendTwo = EnumSet.of(Day.SATURDAY, Day.SUNDAY);
        EnumSet<Day> weekendCopy = EnumSet.copyOf(weekend);
        EnumSet<Day> workingDays = EnumSet.complementOf(weekend);

        boolean result = weekend.addAll(workingDays);
        System.out.println(result);

        EnumSet<Day> overlap = EnumSet.copyOf(workingDays);
        boolean resultOverlap = overlap.retainAll(weekendTwo);


        System.out.println(resultOverlap);

        for (Day day : weekend) {
            System.out.println(day.name());
        }

    }
}
