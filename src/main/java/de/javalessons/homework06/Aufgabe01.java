package de.javalessons.homework06;

import java.util.Scanner;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Написать программу, которая принимает на вход число и
        выводит его в обратном порядке. 11223344-->44332211
         */
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        String numberString = scanner.nextLine();

        int lengthOfNumber = numberString.length();
        System.out.print("Число в обратном порядке :");
        //123456  654...
        for (int i = lengthOfNumber-1; i >=0; i--){
            System.out.print(numberString.charAt(i));
        }

    }
}
