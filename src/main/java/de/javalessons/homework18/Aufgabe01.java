package de.javalessons.homework18;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
        Создайте метод, который принимает переменное
        число аргументов типа int и возвращает их сумму.
         */
        int[] numbers = {12, 20, 35, 47, 55};
        int resultSumNull = printSumOfNumbers(numbers);
        if(resultSumNull == 0){
            System.err.println("Проверьте передаваемые в метод аргументы");
        }
        else {
            System.out.println("Сумма чисел: " + resultSumNull);
        }
    }

    static int printSumOfNumbers(int... numbers) {
        int sum = 0;
        if(numbers == null || numbers.length == 0){
            return sum;
        }
        for (int num : numbers) {
            sum += num;
        }
        return sum;
    }
}
