package de.javalessons.arrays;

public class ArrayUtil {

    public static void main(String[] args) {
        String [] books = new String[5];
        books[0] = "Harry Potter";
       // books[1] = "Harry Potter 2.0";
        System.out.println(books[0]);

        //     0        1           2           3           4
       // "121212",  "3433434",  "3434343",  "34334343",  "232323"; 0
       // "3433434", "4545445",   "454544",  "5555555",   "232323";  1
       // "777878",  "787844",     "7878",   "7873448",   "787878";  2

        //3 строки 5 элементов в каждой
        String [][] pinCodes = new String[3][5];
        pinCodes[0][0] = "121212";
        pinCodes[1][0] = "3433434";
        pinCodes[2][0] = "777878";

        String [][] pinCodesReserve = {{"121212", "3433434", "3434343"},
                {"3433434", "4545445", "454544"},
                {"777878", "787844", "7878"}};

        //System.out.println(pinCodesReserve[0][1]);

        for (int i =0; i < pinCodesReserve.length; i ++)
        {
            for (int y = 0; y < pinCodesReserve[i].length; y++ ){
                System.out.println(pinCodesReserve[i][y]);
            }
        }

    }
}
