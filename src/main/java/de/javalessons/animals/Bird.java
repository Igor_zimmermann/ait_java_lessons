package de.javalessons.animals;

public interface Bird extends Animal {
    void fly();
}
