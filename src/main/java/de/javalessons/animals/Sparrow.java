package de.javalessons.animals;

public class Sparrow extends SmallBird  implements Bird{

    @Override
    public void eat() {
        System.out.println("Sparrow eats");
    }

    @Override
    public void makeSound() {
        System.out.println("Sparrow chirps");
    }

    @Override
    public void fly() {
        System.out.println("Sparrow flies high");
    }
}
