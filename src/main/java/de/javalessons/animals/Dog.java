package de.javalessons.animals;

public class Dog implements Mammal, Pet{
    @Override
    public void eat() {
        System.out.println("Dog eats");
    }

    @Override
    public void makeSound() {
        System.out.println("Woof!!!");
    }

    @Override
    public void feedYoung() {
        System.out.println("DOg feeding the puppies");
    }

    @Override
    public void beFriendly() {
        System.out.println("Dog is playing");
    }
}
