package de.javalessons.homework03;

public class Aufgabe05 {
    /*
    Напишите программу, которая принимает возраст пользователя и
    выводит возрастную категорию: ребенок (до 12 лет),
    подросток (13-18 лет), взрослый (19-59 лет), пожилой (60 лет и старше).
     */

    public static void main(String[] args) {

        int age = 90;

        if (age <= 12 && age > 0) {
            System.out.println("Вы ребенок. Вам " + age + " лет");
        } else if (age >= 13 && age <= 18) {
            System.out.println("Вы подросток. Вам " + age + " лет");
        } else if (age >= 19 && age <= 59) {
            System.out.println("Вы взрослый. Вам " + age + " лет");
        } else if (age >= 60 && age <= 110) {
            System.out.println("Вы пожилой. Вам " + age + " лет");
        } else {
            System.err.println("ОШИБКА!!!");
        }
    }
}
