package de.javalessons.javaio;

public class ScheduleConflictException extends Exception {
    public ScheduleConflictException(String message) {
        super(message);
    }
}
