package de.javalessons.javaio;

import java.io.File;
import java.io.IOException;

public class JavaFiles {
    public static void main(String[] args) throws IOException {
        ///Users/developer/Desktop/fileOne/ -- заменить на свои
        String pathnameFileOne = "Users/developer/Desktop/fileOne/";
        String pathnameFileOneNew = "/Users/developer/Desktop/fileOne/fileOneNew.txt";
        File fileOne = new File(pathnameFileOne);
        File fileOneNew = new File(pathnameFileOneNew);
        fileOne.mkdir();
        File fileTest = new File(fileOne.getAbsolutePath() + "/test.txt");
        System.out.println("Created  fileTest--> " + fileTest.createNewFile());
        fileTest.renameTo(fileOneNew);
        System.out.println("list -->" + fileOne.list());
        for (int i = 0; i < fileOne.list().length; i++) {
            System.out.println(fileOne.list()[i]);
        }

        System.out.println("TotalSpace --> " + fileOne.getTotalSpace());
        System.out.println("FreeSpace -- > " + fileOne.getFreeSpace());
        System.out.println("UsableSpace--> " + fileOne.getUsableSpace());

        String path = fileOne.getPath();
        String pathAbsolute = fileOne.getAbsolutePath();
        System.out.println("Path-->" + path);
        System.out.println("AbsolutePath --> " + pathAbsolute);
        System.out.println("Created --> " + fileOne.createNewFile());
        System.out.println("Name --> " + fileOne.getName());
        System.out.println("isAbsolute->" + fileOne.isAbsolute());
        System.out.println("canRead --" + fileOne.canRead());
        System.out.println("canWrite -->" + fileOne.canWrite());
        System.out.println("exists -->" + fileOne.exists());
        System.out.println("isFile -->" + fileOne.isFile());
        System.out.println("isHidden --> " + fileOne.isHidden());
        System.out.println(fileOne.length());
        if (fileOne.exists() && fileOne.isFile()) {
            //System.out.println("delete -->" + fileOne.delete());
        }


        System.out.println("--------");


        /** File fileTwo = new File("/Users/developer/Documents/DEVELOPMENT/giftbowRepo/JavaLessonAIT/fileTwo.txt");
         String pathfileTwo = fileTwo.getPath();
         String pathAbsolutefileTwo = fileTwo.getAbsolutePath();
         System.out.println("Path fileTwo-->" + pathfileTwo);
         System.out.println("AbsolutePath fileTwo --> " + pathAbsolutefileTwo);
         System.out.println("Created fileTwo --> "+ fileTwo.createNewFile());
         System.out.println(fileTwo.isAbsolute());*/

    }
}
