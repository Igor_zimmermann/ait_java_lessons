package de.javalessons.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DemoThreadRunnable implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoThreadRunnable.class);
    private Thread thread;
    private String threadName;

    public DemoThreadRunnable(String threadName) {
        this.threadName = threadName;
        LOGGER.info("Creating " + threadName);
    }

    public synchronized void start() {
        LOGGER.info("Start {}", threadName);
        if (thread == null) {
            thread = new Thread(this, threadName);
            if (threadName.equals("Thread-1")) {
                thread.setPriority(10);
                thread.setName("Thread-One");
            }
            if (threadName.equals("Thread-2")) {
                thread.setPriority(1);
                thread.setName("Thread-Two");
            }
            thread.start();
        }
    }

    @Override
    public void run() {
        LOGGER.info("Running {}", thread.getName());

        try {
            if (threadName.equals("Thread-2")) {
                thread.join(1000);
            }
            for (int i = 4; i > 0; i--) {
                LOGGER.info("Thread: name:{}, i={}", thread.getName(), i);
                try {
                    Thread.sleep(100);
                    LOGGER.info("Thread name: {} State:{}", thread.getName(), thread.getState().name());
                } catch (InterruptedException exception) {
                    Thread.currentThread().interrupt();
                    LOGGER.error("Thread {} was interrupted {}", thread.getName(), exception);
                }
            }
            LOGGER.info("Thread {} has ended. Is alive? {}", thread.getName(), thread.isAlive());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("Thread {} was interrupted {}", thread.getName(), e);
        }
    }
}