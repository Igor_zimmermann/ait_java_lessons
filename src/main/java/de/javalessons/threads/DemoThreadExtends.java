package de.javalessons.threads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DemoThreadExtends extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoThreadExtends.class);


    private Thread thread;

    private String threadname;

    public DemoThreadExtends(String name) {
        this.threadname = name;
        LOGGER.info("Creating {}", threadname);
    }

    public void run(){
        LOGGER.info("Running {}", thread.getName());
        for (int i = 4; i > 0; i--) {
            LOGGER.info("Thread: name:{}, i={}", thread.getName(), i);
            try {
                Thread.sleep(100);
                LOGGER.info("Thread name: {} State:{}", thread.getName(), thread.getState().name());
            } catch (InterruptedException exception) {
                Thread.currentThread().interrupt();
                LOGGER.error("Thread {} was interrupted", thread.getName(), exception);
            }
        }
        LOGGER.info("Thread {} has ended. Is alive? {}", thread.getName(), thread.isAlive());
        thread.interrupt();
    }

    public void start(){
        LOGGER.info("Start {}", threadname);
        if (thread == null) {
            thread = new Thread(this, threadname);
            thread.start();
        }
    }
}
