package de.javalessons.homework20;

public class Person {
    /*
    Создайте класс Person с приватными полями name и age.
    Добавьте конструктор для инициализации этих полей.
     Добавьте "геттеры" и "сеттеры" для этих полей.
     Модифицируйте класс Person так, чтобы сеттер
     для возраста не позволял устанавливать
     отрицательное значение. Расширьте класс Person
     из первого задания, добавив сообщение об ошибке
     при попытке установить недопустимое значение в
     поле age.
     */

    private  String name;
    private int age;

    public Person(){
    }


    public Person(String name, int age) {
        if(name.length()==0){
            System.err.println("Минимальная длина имени должна быть одной буквой");
        }
        else {
            this.name = name;
        }
        if(age <= 0 || age >=150){
            System.err.println("Данный возраст " + age + " лет недопустим");
        }
        else {
            this.age = age;
        }
    }





    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.length()==0){
            System.err.println("Минимальная длина имени должна быть одной буквой");
        }
        else {
            this.name = name;
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age <= 0 || age >=150){
            System.err.println("Данный возраст " + age + " лет недопустим");
        }
        else {
            this.age = age;
        }
    }
}
