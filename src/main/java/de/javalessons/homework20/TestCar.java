package de.javalessons.homework20;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TestCar {


    public static void main(String[] args) {

        Engine engineBmw = new Engine("1.2TSI", 185);
        Car bmw = new Car("BMW", "X5", engineBmw);


        Engine engine1 = new Engine("рядная 4ка", 45);
        Car car1 = new Car("Ведро", "с гайками", engine1);


        Engine engine2 = new Engine("V12 четыре турбины", 500);
        Car car2 = new Car("Мерин", "голубых кровей", engine2);

        System.out.println("Марка машины: " + car1.getMake() + "\nМодель: " + car1.getModel() + "\nТип двигателя: "
                + car1.getEngine().getType() + "\nМощность двигателя: " + car1.getEngine().getPower());
        System.out.println("---");
        System.out.println("Марка машины: " + car2.getMake() + "\nМодель: " + car2.getModel() + "\nТип двигателя: "
                + car2.getEngine().getType() + "\nМощность двигателя: " + car2.getEngine().getPower());


        System.out.println("---");
        System.out.println("Марка машины: " + car2.getMake() + "\nМодель: " + car2.getModel() + "\nТип двигателя: "
                + car2.getEngine().getType() + "\nМощность двигателя: " + car2.getEngine().getPower());


        ArrayList<Car> garage = new ArrayList<>();
        garage.add(car1);
        garage.add(car2);

        System.out.println(car1);
        System.out.println(car2);




    }
}
