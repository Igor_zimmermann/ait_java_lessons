package de.javalessons.homework57;

import de.javalessons.homework52.AirportFileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class ArrayUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArrayUtil.class);

    public static void main(String[] args) {
        int[][] arrayForTest = createArray(5000, 5000);
        System.out.println(findMaxValue(arrayForTest));
        double average = findAverage(arrayForTest);
        System.out.println(findAverage(arrayForTest));
        showElementsGraterThen(arrayForTest, average);
    }

    public static int[][] createArray(int lineSize, int colSize) {
        if (lineSize <= 0 || colSize <= 0) {
            LOGGER.error("Wrong value in method {} {}", lineSize, colSize);
            throw new NumberFormatException("Wrong value in method");
        }
        int[][] arrayToReturn = new int[lineSize][colSize];
        Random random = new Random();
        for (int i = 0; i < arrayToReturn.length; i++) {
            for (int y = 0; y < arrayToReturn[i].length; y++) {
                int randomInt = random.nextInt(100 + 1);
                arrayToReturn[i][y] = randomInt;
                LOGGER.info("ArrayToReturn Position {} {} Value {}", i, y, randomInt);
            }
        }
        return arrayToReturn;
    }

    public static int findMaxValue(int[][] array) {
        int maxValue = 0;
        for (int i = 0; i < array.length; i++) {
            for (int y = 0; y < array[i].length; y++) {
                if (array[i][y] > maxValue) {
                    maxValue = array[i][y];
                }
            }
        }
        LOGGER.info("Maximum Value {}", maxValue);
        return maxValue;
    }

    public static double findAverage(int[][] array) {
        double sum = 0;
        int count = 0;
        double average = 0;
        for (int i = 0; i < array.length; i++) {
            for (int y = 0; y < array[i].length; y++) {
                count++;
                sum = sum + array[i][y];
            }
        }
        LOGGER.info("Sum {}", sum);
        LOGGER.info("Elements {}", count);
        average = sum / count;
        LOGGER.info("Average: {}", average);
        return average;
    }

    public static void showElementsGraterThen(int[][] array, double number) {
        for (int i = 0; i < array.length; i++) {
            for (int y = 0; y < array[i].length; y++) {
                if (array[i][y] > number) {
                    System.out.println(array[i][y]);
                }
            }
        }
    }

}
