package de.javalessons.homework17;

import java.util.Scanner;

public class Aufgabe01 {
    public static void main(String[] args) {
        /*
         * Создайте метод,
         * который выводит таблицу
         * умножения для заданного числа.
         */
        int numberToMultiplication = readNumber();
        if (numberToMultiplication == 0) {
            System.err.println("Неправильное значение: " + numberToMultiplication);
        } else {
            printMultiplicationTable(numberToMultiplication);
        }
    }

    static int readNumber() {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Введите число (1-10):");
        number = scanner.nextInt();
        if (number >= 1 && number <= 10) {
            scanner.close();
            return number;
        } else {
            System.err.println("Ахтунг! Вы ввели неправильное значение!");
            scanner.close();
            return 0;
        }

    }


    static void printMultiplicationTable(int number) {
        System.out.println("\nТаблица умножения для числа " + number + ":\n");
        for (int i = 1; i <= 10; i++) {
            int result = number * i;
            System.out.println(number + " * " + i + " = " + result);
        }
    }

}
